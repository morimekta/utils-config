Utilities for Configuration
===========================

[![Morimekta](https://img.shields.io/static/v1?label=morimekta.net&message=utils-config&color=informational)](https://morimekta.net/utils-config/)
[![Docs](https://www.javadoc.io/badge/net.morimekta.utils/config.svg)](https://www.javadoc.io/doc/net.morimekta.utils/config)
[![Pipeline](https://gitlab.com/morimekta/utils-config/badges/develop/pipeline.svg)](https://gitlab.com/morimekta/utils-config/pipelines)
[![Coverage](https://gitlab.com/morimekta/utils-config/badges/develop/coverage.svg)](https://morimekta.net/utils-config/jacoco-ut/)
[![License](https://img.shields.io/static/v1?label=license&message=apache%202.0&color=informational)](https://apache.org/licenses/LICENSE-2.0)  
Java module with utilities for managing and loading config and secrets. All of
these should be [kubernetes](https://kubernets.io) safe, handling how it uses
config maps etc.

See [morimekta.net/utils](https://morimekta.net/utils/) for procedures on releases.

## Getting Started

To add to `maven`: Add this line to `pom.xml`under dependencies:

```xml
<dependency>
    <groupId>net.morimekta.utils</groupId>
    <artifactId>config</artifactId>
    <version>${version}</version>
</dependency>
```

To add to `gradle`: Add this line to the `dependencies` group in `build.gradle`:

```
implementation 'net.morimekta.utils:config:${version}'
```

## Configuration

The configuration helpers here have two purposes, simplify the config setup and
allow for in-situ updated configuration. The library supports loading config
from YAML files per default, but can be extended to support more / different
types of config.

- `ConfigSupplier`: A simple supplier class for getting config out of a file.
  Its structure is meant to first set up the supplier (e.g. when setting up
  command line arguments), then load the config (e.g. when parsing the
  arguments). This supplier will only read the config when told to, so is in
  effect a static / fixed config supplier.
- `ConfigWatcher`: Watch a directory for config files of the same type and
  notify listeners on changes (new files, updates and deleted files). It handles
  the parsing of the config and has an event listener for making metrics easy.

```java
import net.morimekta.config.ConfigWatcher;
import net.morimekta.config.readers.YamlConfigReader;

import java.nio.file.Path;

class MyApplication {
  ConfigSupplier<MyAppConfig> supplier = ConfigSupplier.config(MyAppConfig.class);
  ConfigWatcher<MyAppConfig>  watcher  = new ConfigWatcher<MyAppConfig>(
          Path.of("/my/config/dir"),
          new YamlConfigReader<>(MyAppConfig.class),
          config -> config.name);
}
```

### Configuration Readers

- `ConfigReader`: A simple reader interface for getting config out of a
  file.
- `ConfigReaderProvider`: A provider for a config reader. This system is in
  place to allow more different readers to be provided for the config supplier.
  By default, it will support detecting `.yml` and `.yaml` files and parse as
  YAML.
- `YamlConfigReader`: A default config reader implementation for reading YAML
  (1.1) using `snakeyaml`.

## Secrets

One of the important points of this library is to codify the separation of
secrets from config. As secrets must be protected from visibility to some
extent, configuration otherwise **should not** need that protection.

- `SecretsManager`: Manage secrets located in a directory. Update secrets as
  the files are updated. Newly loaded secrets will be available. This is based
  on handling secrets in the standard `kubernetes` way. Most KVM managed secrets
  have a way of building k8s secrets from their own system.
- `Secret`: Keeps a secret, and able to update and listen to updates to the
  secret. If loaded from a YAML config file (jackson), then it can load the
  secret value from an environment variable (env), and if also done after
  setting the secrets config as a context, can also load secrets from the
  secret manager.

### KMS Integrations

How to integrate external sources of secrets is always tricky, but for most
providers there already exist ways of managing `k8s` secrets, resulting in the
same `Secret` or similar resource that can be mounted and used the same way.
The big point here is that your app should not need to care where a secret comes
from, it is always available the same way, and moving from one secret provider
to another should be entirely transparent to the app in question.

In essence anything that can generate native `Secret` resources in `kubernetes`
with a single file for each "secret" to be loaded is supported. And any that
can update the secret in place will allow for in-situ secret updates without
service restarts.

- [`gopass`](https://gopass.pw): Use the gopass kubernetes
  [plugin](https://github.com/gopasspw/kubectl-gopass). To automate deployment
  you can do the updates with [`flux`](https://fluxcd.io) using the
  [flux-secret](https://github.com/caos/flux-secret) plugin.
- [`HashiCorp Vault`](https://www.vaultproject.io/): Use the Vault
  [kubernetes-sidecar](https://developer.hashicorp.com/vault/tutorials/kubernetes/kubernetes-sidecar)
  to load and update the secrets.
- [`AWS Managed Secret`](https://aws.amazon.com/secrets-manager/): There is an AWS official
  [driver](https://docs.aws.amazon.com/secretsmanager/latest/userguide/integrating_csi_driver.html)
  that can load secrets from the AWS secrets into your EKS cluster.

Additionally, some password managers may be used to manage secrets used in some
services:

- [`1password`](https://1password.com/): There is a
  [kubernetes injector](https://developer.1password.com/docs/connect/k8s-injector)
  for handling secrets from `1password`.
- [`LastPass`](https://lastpass.com): There is a [LastPass Operator](https://github.com/edgelevel/lastpass-operator)
  that can create and manage secrets from a lastpass account.
