/*
 * Copyright 2023 Morimekta Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.config;

import java.util.Locale;

import static net.morimekta.strings.StringUtil.capitalize;

/**
 * Types of changes on config content.
 */
public enum ConfigChangeType {
    /** The config entry has just been created. */
    CREATED("creating"),
    /** The config entry has been modified from a previously known state. */
    MODIFIED("modifying"),
    /** The config entry has been deleted. */
    DELETED("deleting"),
    ;

    ConfigChangeType(String action) {
        this.action = action;
    }

    /**
     * One of 'created', 'modified' or 'deleted'.
     *
     * @return The config change name as lowercase.
     */
    public String lowercase() {
        return name().toLowerCase(Locale.US);
    }

    /**
     * One of 'Created', 'Modified' or 'Deleted'.
     *
     * @return The config change name, capitalized, for e.g. where needed first in sentence.
     */
    public String capitalized() {
        return capitalize(lowercase());
    }

    /**
     * One of 'creating', 'modifying' or 'deleting'.
     *
     * @return The config action (as lowercase).
     */
    public String action() {
        return action;
    }

    /**
     * One of 'Creating', 'Modifying' or 'Deleting'.
     *
     * @return The config action, capitalized, for e.g. where needed first in sentence.
     */
    public String capitalizedAction() {
        return capitalize(action);
    }

    private final String action;
}
