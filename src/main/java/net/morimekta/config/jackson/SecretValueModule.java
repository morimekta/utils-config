package net.morimekta.config.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;

public class SecretValueModule extends SimpleModule {
    public SecretValueModule() {
        super("SecretValueModule");
        addDeserializer(String.class, new SecretValueDeserializer());
    }
}
