/*
 * Copyright 2023 Morimekta Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.config;

import java.util.concurrent.atomic.AtomicReference;

import static net.morimekta.config.ConfigException.asConfigException;

/**
 * A small utility buffer class for accumulating exceptions and passing as
 * a config exception if any happens.
 */
public class ConfigExceptionBuffer {
    /**
     * Add exception to the buffer.
     *
     * @param e Exception to add.
     */
    public void update(Exception e) {
        ex.updateAndGet(old -> {
            if (old == null) {
                // update -> ex.updateAndGet -> {lambda} -> asConfigException
                return asConfigException(e, 4);
            }
            old.addSuppressed(e);
            return old;
        });
    }

    /**
     * Throw the accumulated exception, if it has an exception.
     *
     * @throws ConfigException The accumulated exception if present.
     */
    public void throwIfPresent() throws ConfigException {
        if (ex.get() != null) {
            throw ex.get();
        }
    }

    // *****************
    // **   PRIVATE   **
    // *****************

    private final AtomicReference<ConfigException> ex = new AtomicReference<>();
}
