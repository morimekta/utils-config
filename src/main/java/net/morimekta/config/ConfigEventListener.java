/*
 * Copyright 2023 Morimekta Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.config;

import net.morimekta.file.FileEvent;

import java.nio.file.Path;

/**
 * Listen to config events. This can handle metrics for the config
 * watcher.
 */
public interface ConfigEventListener {
    /**
     * The status of the event.
     */
    enum Status {
        /** The operation succeeded. */
        OK,
        /** Reading a config file failed. */
        READ_FAILED,
        /** Parsing a config file failed. */
        PARSE_FAILED,
        /** Error on config change update. */
        ERROR
    }

    /**
     * Called when the config file events are triggered. Usually
     * all files are modified at the same time, this is also on
     * all files created and deleted.
     *
     * @param fileEvent The file event.
     * @param filePath  The file affected.
     * @param status    The config file read status.
     */
    void onConfigFileRead(FileEvent fileEvent,
                          Path filePath,
                          Status status);

    /**
     * Called on listeners whenever the content of a config is changed.
     * This is checked using {@link Object#equals(Object)} on the config
     * instance parsed for the same file path. The status is based on the
     * result of calling the config change listeners. If any threw an
     * exception, it is handled as ERROR, otherwise as OK.
     *
     * @param changeType The config change type.
     * @param filePath   The file affected.
     * @param identifier The config's identifier, or filename if none.
     * @param status     The config change status.
     */
    void onConfigFileUpdate(ConfigChangeType changeType,
                            Path filePath,
                            String identifier,
                            Status status);
}
