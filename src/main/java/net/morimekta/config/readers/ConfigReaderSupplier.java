/*
 * Copyright 2023 Morimekta Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.config.readers;

import net.morimekta.config.ConfigException;

import java.nio.file.Path;

/**
 * A helper class to load config files from disk and parse into
 * the specified config type.
 *
 * @param <ConfigType> The config type to get reader for.
 */
public interface ConfigReaderSupplier<ConfigType> {
    /**
     * @param file File path to get reader for.
     * @return The config reader to read given file.
     * @throws ConfigException If unable to get suitable reader.
     */
    ConfigReader<ConfigType> getReaderFor(Path file) throws ConfigException;
}
