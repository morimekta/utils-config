/*
 * Copyright 2023 Morimekta Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.config.readers;

import java.nio.file.Path;
import java.util.Optional;

/**
 * Provider of a single type of config readers. Set up with creating the file:
 * <pre>{@code
 * META-INF/services/net.morimekta.config.readers.ConfigReaderProvider
 * }</pre>
 * And containing the class path of your provider, and to add the provider
 * as provided in the <code>module-info.java</code> as such:
 * <pre>{@code
 * provides net.morimekta.config.readers.ConfigReaderProvider
 *     with your.provider.ClassPath;
 * }</pre>
 */
public interface ConfigReaderProvider {
    /**
     * Get a config reader for given type and file, if applicable.
     *
     * @param type         The config type to get reader for.
     * @param file         The file to get reader for.
     * @param <ConfigType> The config type.
     * @return The provided reader, or empty optional if not applicable.
     */
    <ConfigType> Optional<ConfigReader<ConfigType>> getReaderFor(Class<ConfigType> type, Path file);
}
