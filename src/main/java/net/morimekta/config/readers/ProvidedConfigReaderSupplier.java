/*
 * Copyright 2023 Morimekta Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.config.readers;

import net.morimekta.collect.util.LazyCachedSupplier;
import net.morimekta.config.ConfigException;

import java.nio.file.Path;
import java.util.ServiceLoader;

/**
 * Get config readers from the providers providing ConfigReaders.
 */
public final class ProvidedConfigReaderSupplier<ConfigType> implements ConfigReaderSupplier<ConfigType> {
    /**
     * Makea config reader service to provide readers for the specific type.
     *
     * @param configType The config type to get readers for.
     */
    public ProvidedConfigReaderSupplier(Class<ConfigType> configType) {
        this.configType = configType;
    }

    @Override
    public ConfigReader<ConfigType> getReaderFor(Path file) throws ConfigException {
        for (ConfigReaderProvider provider : kServiceLoader.get()) {
            var reader = provider.getReaderFor(configType, file);
            if (reader.isPresent()) {
                return reader.get();
            }
        }
        throw new ConfigException(
                "No config reader available for " + file.getFileName() +
                " of type " + configType.getName());
    }

    private final Class<ConfigType> configType;

    private static final LazyCachedSupplier<ServiceLoader<ConfigReaderProvider>> kServiceLoader =
            new LazyCachedSupplier<>(() -> ServiceLoader.load(ConfigReaderProvider.class));
}
