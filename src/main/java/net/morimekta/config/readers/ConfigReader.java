/*
 * Copyright 2023 Morimekta Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.config.readers;

import net.morimekta.config.ConfigException;

import java.io.IOException;
import java.nio.file.Path;

/**
 * A interface to load config files from disk and parse into
 * the specified config type.
 *
 * @param <ConfigType> The config type.
 */
public interface ConfigReader<ConfigType> {
    /**
     * @param file File path to load config from.
     * @return The loaded config file.
     * @throws ConfigException If parsing file failed.
     * @throws IOException     If reading the config file failed.
     */
    ConfigType readConfig(Path file) throws IOException, ConfigException;
}
