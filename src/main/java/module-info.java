/**
 * <b>Utils : Config</b>
 */
module net.morimekta.config {
    exports net.morimekta.config;
    exports net.morimekta.config.readers;
    exports net.morimekta.config.jackson;

    uses net.morimekta.config.readers.ConfigReaderProvider;
    provides net.morimekta.config.readers.ConfigReaderProvider
            with net.morimekta.config.readers.YamlConfigReaderProvider;
    provides com.fasterxml.jackson.databind.Module
            with net.morimekta.config.jackson.SecretValueModule;

    requires net.morimekta.collect;
    requires net.morimekta.file;
    requires net.morimekta.strings;

    requires org.slf4j;
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.dataformat.yaml;
}