package net.morimekta.config;

import org.junit.jupiter.api.Test;

import static net.morimekta.config.ConfigException.asConfigException;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigExceptionTest {
    @Test
    public void testAsConfigException() {
        ConfigException ex = new ConfigException("test");
        assertThat(asConfigException(ex), is(sameInstance(ex)));
        RuntimeException e2 = new RuntimeException("null");
        ConfigException c2 = asConfigException(e2);
        assertThat(c2.getMessage(), is(e2.getMessage()));
        assertThat(c2.getCause(), is(sameInstance(e2)));
    }

    @Test
    public void testAsUncheckedException() {
        ConfigException ex = new ConfigException("test");
        UncheckedConfigException uex = ex.asUncheckedException();
        assertThat(uex.getMessage(), is(ex.getMessage()));
        assertThat(uex.getCause(), is(ex));
    }
}
