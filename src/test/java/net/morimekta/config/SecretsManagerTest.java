package net.morimekta.config;

import com.fasterxml.jackson.databind.JsonMappingException;
import net.morimekta.collect.util.Binary;
import net.morimekta.file.FileEvent;
import net.morimekta.file.FileEventListener;
import net.morimekta.file.FileWatcher;
import net.morimekta.testing.file.TestConfigMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.NoSuchElementException;
import java.util.Set;

import static java.nio.file.Files.createDirectory;
import static java.nio.file.Files.writeString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class SecretsManagerTest {
    @TempDir
    public Path secretsDir;
    @TempDir
    public Path configDir;

    @Test
    public void testSecretsManager() throws IOException, InterruptedException {
        var configMap = new TestConfigMap(secretsDir);
        try (var update = configMap.update()) {
            writeString(
                    update.path("TEST"),
                    "PASSWORD");
            writeString(
                    update.path("OTHER"),
                    "p4ssw0rd");
        }

        try (var secrets = new SecretsManager(secretsDir)) {
            assertThat(secrets.getKnownSecrets(), is(Set.of("TEST", "OTHER")));
            assertThat(secrets.getSecretsPath(), is(secretsDir));
            assertThat(secrets.toString(), is("SecretsManager{dir=" + secretsDir + "}"));

            assertThat(secrets.get("TEST").getName(), is("TEST"));
            assertThat(secrets.getAsPath("TEST"), is(secretsDir.resolve("TEST")));
            assertThat(secrets.getAsString("TEST"), is("PASSWORD"));
            assertThat(secrets.getAsBytes("TEST"), is(new byte[]{80, 65, 83, 83, 87, 79, 82, 68}));
            assertThat(secrets.getAsBinary("TEST"), is(Binary.fromHexString("50415353574f5244")));

            try (var update = configMap.update()) {
                writeString(
                        update.path("TEST"),
                        "PASSWORD-2");
            }

            Thread.sleep(100);

            assertThat(secrets.getAsString("TEST"), is("PASSWORD-2"));
        }
    }

    @Test
    public void testSecretsManager_FromConfigObject() throws IOException, ConfigException {
        writeString(
                secretsDir.resolve("OTHER"),
                "p4ssw0rd");
        writeString(
                configDir.resolve("test-config.yml"),
                "secretsManager:\n" +
                "  dir: \"" + secretsDir.toString() + "\"\n" +
                "unknown: \"OTHER\"\n");

        var loader = ConfigSupplier.yamlConfig(TestConfig.class);
        loader.load(configDir.resolve("test-config.yml"));
        var loaded = loader.get();
        assertThat(loaded, is(notNullValue()));
        assertThat(loaded.secretsManager, is(notNullValue()));
        assertThat(loaded.secretsManager.getSecretsPath(), is(secretsDir));
        assertThat(loaded.unknown.getName(), is("OTHER"));
        assertThat(loaded.unknown.getAsString(), is("p4ssw0rd"));
        // close it.
        loaded.secretsManager.close();
    }

    @Test
    public void testSecretsManager_WatcherEventOnUnknownSecret() throws IOException {
        // testing supremely unlikely scenarios, so need to mock the file watcher.
        var fileWatcher = Mockito.mock(FileWatcher.class, "FileWatcher");
        try (var manager = new SecretsManager(secretsDir, fileWatcher)) {
            verifyNoInteractions(fileWatcher);
            writeString(secretsDir.resolve("SECRET"), "HIDE");
            writeString(secretsDir.resolve("OTHER"), "HIDE");

            var secret = manager.get("SECRET");
            assertThat(secret, is(notNullValue()));

            var listenerCaptor = ArgumentCaptor.forClass(FileEventListener.class);
            verify(fileWatcher).weakAddWatcher(eq(secretsDir.resolve("SECRET")), listenerCaptor.capture());
            verifyNoMoreInteractions(fileWatcher);
            reset(fileWatcher);

            var listener = listenerCaptor.getValue();
            listener.onFileEvent(secretsDir.resolve("OTHER"), FileEvent.CREATED);

            verify(fileWatcher).removeWatcher(secretsDir.resolve("OTHER"), listener);
            verifyNoMoreInteractions(fileWatcher);
        }
    }

    @Test
    public void testSecretsManager_WatcherEventOnSecretAsDeadSymlink() throws IOException {
        // testing supremely unlikely scenarios, so need to mock the file watcher.
        var fileWatcher = Mockito.mock(FileWatcher.class, "FileWatcher");
        try (var manager = new SecretsManager(secretsDir, fileWatcher)) {
            verifyNoInteractions(fileWatcher);
            writeString(secretsDir.resolve("SECRET"), "HIDE");

            var secret = manager.get("SECRET");
            assertThat(secret, is(notNullValue()));
            assertThat(secret.getAsString(), is("HIDE"));

            var listenerCaptor = ArgumentCaptor.forClass(FileEventListener.class);
            verify(fileWatcher).weakAddWatcher(eq(secretsDir.resolve("SECRET")), listenerCaptor.capture());
            verifyNoMoreInteractions(fileWatcher);
            reset(fileWatcher);

            Files.delete(secretsDir.resolve("SECRET"));
            Files.createSymbolicLink(secretsDir.resolve("SECRET"), Path.of("THIRD"));

            var listener = listenerCaptor.getValue();
            listener.onFileEvent(secretsDir.resolve("SECRET"), FileEvent.CREATED);

            verify(fileWatcher).removeWatcher(secretsDir.resolve("SECRET"), listener);
            verifyNoMoreInteractions(fileWatcher);
        }
    }

    @Test
    public void testSecretsManager_SecretIsDir() throws IOException {
        // testing supremely unlikely scenarios, so need to mock the file watcher.
        var fileWatcher = Mockito.mock(FileWatcher.class, "FileWatcher");
        try (var manager = new SecretsManager(secretsDir, fileWatcher)) {
            verifyNoInteractions(fileWatcher);
            createDirectory(secretsDir.resolve("SECRET"));

            try {
                fail("No exception: " + manager.get("SECRET"));
            } catch (NoSuchElementException e) {
                assertThat(e.getMessage(), is("Secret SECRET is not a file."));
            }
        }
    }

    @Test
    public void testSecretsManager_FromConfigObject_NoDir() throws IOException {
        writeString(
                configDir.resolve("test-config.yml"),
                "secretsManager:\n" +
                "  dir: \"" + secretsDir.toString() + "/foo\"\n");

        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class)) {
            loader.load(configDir.resolve("test-config.yml"));
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is(
                    "Secrets location \"" + secretsDir + "/foo\" not a directory.\n" +
                    " at [Source: (BufferedReader); line: 2, column: 3]" +
                    " (through reference chain: net.morimekta.config.TestConfig[\"secretsManager\"])"));
            assertThat(e.getCause(), is(instanceOf(JsonMappingException.class)));
        }
    }

    @Test
    public void testSecretsManager_FromConfigObject_Invalid() throws IOException {
        writeString(
                configDir.resolve("test-config.yml"),
                "secretsManager:\n" +
                "  dir:\n" +
                "    - 42\n");

        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class);) {
            loader.load(configDir.resolve("test-config.yml"));
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is(
                    "Cannot deserialize value of type `java.nio.file.Path` from Array value (token `JsonToken.START_ARRAY`)\n" +
                    " at [Source: (BufferedReader); line: 3, column: 5]" +
                    " (through reference chain: net.morimekta.config.TestConfig[\"secretsManager\"]" +
                    "->net.morimekta.config.SecretsManager$SecretsConfig[\"dir\"])"));
            assertThat(e.getCause(), is(instanceOf(JsonMappingException.class)));
        }
    }

    @Test
    public void testSecretsManager_FromPath_NoDir() throws IOException {
        writeString(
                configDir.resolve("test-config.yml"),
                "secretsManager: \"" + secretsDir.toString() + "/foo\"\n");

        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class)) {
            loader.load(configDir.resolve("test-config.yml"));
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is(
                    "Secrets location \"" + secretsDir + "/foo\" not a directory.\n" +
                    " at [Source: (BufferedReader); line: 1, column: 17]" +
                    " (through reference chain: net.morimekta.config.TestConfig[\"secretsManager\"])"));
        }
    }

    @Test
    public void testSecretsManager_Invalid() throws IOException {
        writeString(
                configDir.resolve("test-config.yml"),
                "secretsManager: 42\n");

        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class);) {
            loader.load(configDir.resolve("test-config.yml"));
            fail("no exception");
        } catch (ConfigException e) {
            assertThat(e.getMessage(), is(
                    "Invalid SecretManager config value: 42\n" +
                    "Must either be a directory path as string, or a config object with a \"dir\" property.\n" +
                    " at [Source: (BufferedReader); line: 1, column: 17]" +
                    " (through reference chain: net.morimekta.config.TestConfig[\"secretsManager\"])"));
        }
    }

    @Test
    public void testSecretDirDeleted() throws IOException {
        var dir = secretsDir.resolve("sub");
        Files.createDirectory(dir);
        try (var secrets = new SecretsManager(dir)) {
            Files.delete(dir);
            assertThat(secrets.exists("foo"), is(false));
            try {
                fail("no exception: " + secrets.get("foo"));
            } catch (IllegalStateException e) {
                assertThat(e.getMessage(), is("Secrets directory deleted: \"" + secretsDir + "/sub\"."));
            }
            assertThat(secrets.getKnownSecrets(), is(Set.of()));
        }
    }

    @Test
    public void testGetInvalidName() throws IOException {
        try (var secrets = new SecretsManager(secretsDir)) {
            try {
                fail("no exception: " + secrets.get(null));
            } catch (NullPointerException e) {
                assertThat(e.getMessage(), is("name == null"));
            }
            try {
                fail("no exception: " + secrets.get(".bar"));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Hidden file or path in name, not allowed: \".bar\"."));
            }
            try {
                fail("no exception: " + secrets.get(""));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Empty secret name."));
            }
        }
    }

    @Test
    public void testExistsInvalidName() throws IOException {
        try (var secrets = new SecretsManager(secretsDir)) {
            try {
                fail("no exception: " + secrets.exists(null));
            } catch (NullPointerException e) {
                assertThat(e.getMessage(), is("name == null"));
            }
            try {
                fail("no exception: " + secrets.exists(".bar"));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Hidden file or path in name, not allowed: \".bar\"."));
            }
            try {
                fail("no exception: " + secrets.exists(""));
            } catch (IllegalArgumentException e) {
                assertThat(e.getMessage(), is("Empty secret name."));
            }
        }
    }

    @Test
    public void testGetAsPathDoesNotExist() throws IOException {
        try (var secrets = new SecretsManager(secretsDir)) {
            try {
                fail("no exception: " + secrets.getAsPath("FOO"));
            } catch (NoSuchElementException e) {
                assertThat(e.getMessage(), is("No secret with name \"FOO\""));
            }
        }
    }
}
