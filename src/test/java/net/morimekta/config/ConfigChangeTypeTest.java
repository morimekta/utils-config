package net.morimekta.config;

import org.junit.jupiter.api.Test;

import static net.morimekta.config.ConfigChangeType.CREATED;
import static net.morimekta.config.ConfigChangeType.DELETED;
import static net.morimekta.config.ConfigChangeType.MODIFIED;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigChangeTypeTest {
    @Test
    public void testConfigChangeStrings() {
        assertThat(CREATED.lowercase(), is("created"));
        assertThat(MODIFIED.lowercase(), is("modified"));
        assertThat(DELETED.lowercase(), is("deleted"));

        assertThat(CREATED.capitalized(), is("Created"));
        assertThat(MODIFIED.capitalized(), is("Modified"));
        assertThat(DELETED.capitalized(), is("Deleted"));

        assertThat(CREATED.action(), is("creating"));
        assertThat(MODIFIED.action(), is("modifying"));
        assertThat(DELETED.action(), is("deleting"));

        assertThat(CREATED.capitalizedAction(), is("Creating"));
        assertThat(MODIFIED.capitalizedAction(), is("Modifying"));
        assertThat(DELETED.capitalizedAction(), is("Deleting"));
    }
}
