package net.morimekta.config;

import java.util.Objects;

public class TestConfig {
    public String name;
    public String value;

    public SecretsManager secretsManager;
    public Secret         secret;
    public Secret         nullValue;
    public Secret         fromEnv;
    public Secret         unknown;

    public String secretEnv;
    public String secretValue;
    public String nullSecret;
    public String rawSecret;

    public TestConfig() {}

    public TestConfig(String name, String value) {
        this.name = name;
        this.value = value;
    }

    @Override
    public String toString() {
        var builder = new StringBuilder().append("TestConfig{");
        var first = true;
        if (name != null) {
            first = false;
            builder.append("name='").append(name).append('\'');
        }
        if (value != null) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append("value='").append(value).append('\'');
        }
        if (secretsManager != null) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append("secretsManager=").append(secretsManager);
        }
        if (secret != null) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append("secret=").append(secret);
        }
        if (nullValue != null) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append("nullValue=").append(nullValue);
        }
        if (fromEnv != null) {
            if (first) {
                first = false;
            } else {
                builder.append(",");
            }
            builder.append("fromEnv=").append(fromEnv);
        }
        if (unknown != null) {
            if (!first) {
                builder.append(",");
            }
            builder.append("unknown=").append(unknown);
        }
        return builder.append('}').toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        TestConfig that = (TestConfig) o;
        return Objects.equals(name, that.name) &&
               Objects.equals(value, that.value) &&
               Objects.equals(secretsManager, that.secretsManager) &&
               Objects.equals(secret, that.secret) &&
               Objects.equals(nullValue, that.nullValue) &&
               Objects.equals(fromEnv, that.fromEnv) &&
               Objects.equals(unknown, that.unknown);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value, secretsManager, secret, nullValue, fromEnv, unknown);
    }
}