package net.morimekta.config;

import com.fasterxml.jackson.databind.JsonMappingException;
import net.morimekta.config.readers.YamlConfigReader;
import net.morimekta.file.FileEvent;
import net.morimekta.testing.concurrent.FakeClock;
import net.morimekta.testing.file.TestConfigMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.nio.file.Path;
import java.util.Set;
import java.util.stream.Collectors;

import static net.morimekta.config.ConfigEventListener.Status.ERROR;
import static net.morimekta.config.ConfigEventListener.Status.OK;
import static net.morimekta.config.ConfigEventListener.Status.PARSE_FAILED;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ConfigWatcherTest {
    @TempDir
    public Path tmp;

    @Test
    public void testConfigWatcher() throws IOException, ConfigException {
        var cm = new TestConfigMap(tmp);
        try (var update = cm.update()) {
            update.writeContent(
                    "config.yaml",
                    "name: \"foo\"\n" +
                    "value: \"bar\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        var clock = FakeClock.forCurrentTimeMillis(1234567890123L);
        try (var watcher = new ConfigWatcher<>(
                tmp,
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name,
                clock)
                .addEventListener(eventListener)
                .addChangeListener(changeListener)
                .start()) {
            assertThat(watcher.toString(), is("ConfigWatcher{dir=" + tmp + "}"));

            var config = ArgumentCaptor.forClass(TestConfig.class);

            verify(changeListener, timeout(250)).onConfigChange(eq(ConfigChangeType.CREATED), config.capture());
            assertThat(config.getValue().name, is("foo"));
            assertThat(config.getValue().value, is("bar"));
            verify(eventListener).onConfigFileRead(FileEvent.CREATED, tmp.resolve("config.yaml"), OK);
            verify(eventListener).onConfigFileUpdate(ConfigChangeType.CREATED, tmp.resolve("config.yaml"), "foo", OK);
            verifyNoMoreInteractions(changeListener, eventListener);
            reset(changeListener, eventListener);

            try (var update = cm.update()) {
                update.delete("config.yaml");
                update.writeContent(
                        "config.yaml",
                        "name: \"foo\"\n" +
                        "value: \"baz\"\n");
                update.writeContent(
                        "other.yaml",
                        "name: \"fizz\"\n" +
                        "value: \"buzz\"\n");
            }

            var config2 = ArgumentCaptor.forClass(TestConfig.class);
            var config3 = ArgumentCaptor.forClass(TestConfig.class);
            verify(changeListener, timeout(250)).onConfigChange(eq(ConfigChangeType.CREATED), config2.capture());
            verify(changeListener, timeout(250)).onConfigChange(eq(ConfigChangeType.MODIFIED), config3.capture());
            verify(eventListener, atLeastOnce()).onConfigFileRead(any(FileEvent.class),
                                                                  eq(tmp.resolve("other.yaml")),
                                                                  eq(OK));
            verify(eventListener, atLeastOnce()).onConfigFileRead(any(FileEvent.class),
                                                                  eq(tmp.resolve("config.yaml")),
                                                                  eq(OK));
            verify(eventListener).onConfigFileUpdate(ConfigChangeType.CREATED, tmp.resolve("other.yaml"), "fizz", OK);
            verify(eventListener).onConfigFileUpdate(ConfigChangeType.MODIFIED, tmp.resolve("config.yaml"), "foo", OK);
            verifyNoMoreInteractions(changeListener, eventListener);
            reset(changeListener, eventListener);

            assertThat(config2.getValue().name, is("fizz"));
            assertThat(config2.getValue().value, is("buzz"));
            assertThat(config3.getValue().name, is("foo"));
            assertThat(config3.getValue().value, is("baz"));

            assertThat(watcher.getConfigDir(), is(tmp));
            var map = watcher.getLoadedConfigMap();
            assertThat(map.keySet(),
                       is(Set.of(tmp.resolve("config.yaml"), tmp.resolve("other.yaml"))));
            assertThat(map.values()
                          .stream()
                          .map(e -> e.file)
                          .collect(Collectors.toSet()),
                       is(Set.of(tmp.resolve("config.yaml"), tmp.resolve("other.yaml"))));
            assertThat(map.values()
                          .stream()
                          .map(e -> e.identifier)
                          .collect(Collectors.toSet()),
                       is(Set.of("foo", "fizz")));
            assertThat(map.values()
                          .stream()
                          .map(e -> e.updatedAt)
                          .collect(Collectors.toSet()),
                       is(Set.of(clock.instant())));

            try (var update = cm.update()) {
                update.delete("config.yaml");
            }

            var config4 = ArgumentCaptor.forClass(TestConfig.class);
            verify(changeListener, timeout(250)).onConfigChange(eq(ConfigChangeType.DELETED), config4.capture());

            verify(eventListener, timeout(250).atLeastOnce()).onConfigFileRead(
                    FileEvent.MODIFIED, tmp.resolve("other.yaml"), OK);
            verify(eventListener, timeout(250)).onConfigFileRead(
                    FileEvent.DELETED, tmp.resolve("config.yaml"), OK);

            verify(eventListener).onConfigFileUpdate(
                    ConfigChangeType.DELETED, tmp.resolve("config.yaml"), "foo", OK);

            verifyNoMoreInteractions(changeListener, eventListener);

            // the config in the listener call is the last read and parsed.
            assertThat(config4.getValue(), is(config3.getValue()));
        }
    }

    @Test
    public void testConfigWatcher_NotADir() {
        try (var ignore = new ConfigWatcher<>(
                tmp.resolve("404"),
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name)) {
            fail("No exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a directory: " + tmp.resolve("404")));
        }
    }

    @Test
    public void testInvalidParseOnStart() throws IOException {
        var cm = new TestConfigMap(tmp);
        try (var update = cm.update()) {
            update.writeContent(
                    "config.yaml",
                    "name:\n" +
                    "  - 42\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        try (var watcher = new ConfigWatcher<>(
                tmp,
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name)) {
            watcher.addEventListener(eventListener);
            watcher.addChangeListener(changeListener);
            try {
                watcher.start();
                fail("No exception");
            } catch (ConfigException e) {
                assertThat(e.getMessage(), is(
                        "Cannot deserialize value of type `java.lang.String` from Array value (token `JsonToken.START_ARRAY`)\n" +
                        " at [Source: (BufferedReader); line: 2, column: 3]" +
                        " (through reference chain: net.morimekta.config.TestConfig[\"name\"])"));
                assertThat(e.getCause(), is(instanceOf(JsonMappingException.class)));
            }
        }
    }

    @Test
    public void testInvalidParseOnUpdate() throws IOException, ConfigException {
        var cm = new TestConfigMap(tmp);
        try (var update = cm.update()) {
            update.writeContent(
                    "config.yaml",
                    "name: \"foo\"\n" +
                    "value: \"bar\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        try (var watcher = new ConfigWatcher<>(
                tmp,
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name)) {
            watcher.addEventListener(eventListener);
            watcher.addChangeListener(changeListener);
            watcher.start();

            var config = ArgumentCaptor.forClass(TestConfig.class);

            verify(changeListener, timeout(250)).onConfigChange(eq(ConfigChangeType.CREATED), config.capture());
            assertThat(config.getValue().name, is("foo"));
            assertThat(config.getValue().value, is("bar"));
            verify(eventListener).onConfigFileRead(FileEvent.CREATED, tmp.resolve("config.yaml"), OK);
            verify(eventListener).onConfigFileUpdate(ConfigChangeType.CREATED, tmp.resolve("config.yaml"), "foo", OK);
            verifyNoMoreInteractions(changeListener, eventListener);
            reset(changeListener, eventListener);

            try (var update = cm.update()) {
                update.delete("config.yaml");
                update.writeContent(
                        "config.yaml",
                        "name:\n" +
                        "  - 42\n");
            }

            verify(eventListener, timeout(250).atLeastOnce()).onConfigFileRead(
                    any(), eq(tmp.resolve("config.yaml")), eq(PARSE_FAILED));
            verifyNoMoreInteractions(changeListener, eventListener);
        }
    }

    @Test
    public void testUpdateException_OnStart() throws IOException, ConfigException {
        var cm = new TestConfigMap(tmp);
        try (var update = cm.update()) {
            update.writeContent(
                    "config.yaml",
                    "name: \"foo\"\n" +
                    "value: \"bar\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        doThrow(new IllegalArgumentException("foo"))
                .when(changeListener)
                .onConfigChange(eq(ConfigChangeType.CREATED), any(TestConfig.class));
        var eventListener = mock(ConfigEventListener.class);
        try (var watcher = new ConfigWatcher<>(
                tmp,
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name)) {
            watcher.addEventListener(eventListener);
            watcher.addChangeListener(changeListener);
            try {
                watcher.start();
                fail("No exception");
            } catch (ConfigException e) {
                assertThat(e.getMessage(), is("foo"));
                assertThat(e.getCause(), is(instanceOf(IllegalArgumentException.class)));
            }
        }
    }

    @Test
    public void testUpdateException_OnUpdate() throws IOException, ConfigException {
        var cm = new TestConfigMap(tmp);
        try (var update = cm.update()) {
            update.writeContent(
                    "config.yaml",
                    "name: \"foo\"\n" +
                    "value: \"bar\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener1 = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        @SuppressWarnings("unchecked")
        var changeListener2 = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        try (var watcher = new ConfigWatcher<>(
                tmp,
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name)) {
            watcher.addEventListener(eventListener);
            watcher.addChangeListener(changeListener1);
            watcher.addChangeListener(changeListener2);
            watcher.start();

            verify(changeListener1, timeout(250)).onConfigChange(eq(ConfigChangeType.CREATED), any());
            verify(changeListener2, timeout(250)).onConfigChange(eq(ConfigChangeType.CREATED), any());
            verify(eventListener).onConfigFileRead(FileEvent.CREATED, tmp.resolve("config.yaml"), OK);
            verify(eventListener).onConfigFileUpdate(ConfigChangeType.CREATED, tmp.resolve("config.yaml"), "foo", OK);
            verifyNoMoreInteractions(changeListener1, eventListener);
            reset(changeListener1, changeListener2, eventListener);

            doThrow(new IllegalArgumentException("foo"))
                    .when(changeListener1)
                    .onConfigChange(eq(ConfigChangeType.MODIFIED), any(TestConfig.class));
            doThrow(new IllegalArgumentException("bar"))
                    .when(changeListener2)
                    .onConfigChange(eq(ConfigChangeType.MODIFIED), any(TestConfig.class));
            doThrow(new IllegalArgumentException("fizz"))
                    .when(eventListener)
                    .onConfigFileRead(any(), any(), any());
            doThrow(new IllegalArgumentException("buzz"))
                    .when(eventListener)
                    .onConfigFileUpdate(any(), any(), anyString(), any());

            try (var update = cm.update()) {
                update.delete("config.yaml");
                update.writeContent(
                        "config.yaml",
                        "name: \"foo\"\n" +
                        "value: \"buzz\"\n");
            }

            verify(changeListener1, timeout(250)).onConfigChange(eq(ConfigChangeType.MODIFIED), any());
            verify(changeListener2, timeout(250)).onConfigChange(eq(ConfigChangeType.MODIFIED), any());
            verify(eventListener, atLeastOnce()).onConfigFileRead(
                    FileEvent.MODIFIED, tmp.resolve("config.yaml"), OK);
            verify(eventListener).onConfigFileUpdate(
                    ConfigChangeType.MODIFIED, tmp.resolve("config.yaml"), "foo", ERROR);
            verifyNoMoreInteractions(changeListener1, changeListener2, eventListener);
        }
    }

    @Test
    public void testUpdateException_OnDelete() throws IOException, ConfigException {
        var cm = new TestConfigMap(tmp);
        try (var update = cm.update()) {
            update.writeContent(
                    "config.yaml",
                    "name: \"foo\"\n" +
                    "value: \"bar\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        try (var watcher = new ConfigWatcher<>(
                tmp,
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name)) {
            watcher.addEventListener(eventListener);
            watcher.addChangeListener(changeListener);
            watcher.start();

            var config = ArgumentCaptor.forClass(TestConfig.class);

            verify(changeListener, timeout(250)).onConfigChange(eq(ConfigChangeType.CREATED), config.capture());
            assertThat(config.getValue().name, is("foo"));
            assertThat(config.getValue().value, is("bar"));
            verify(eventListener).onConfigFileRead(FileEvent.CREATED, tmp.resolve("config.yaml"), OK);
            verify(eventListener).onConfigFileUpdate(ConfigChangeType.CREATED, tmp.resolve("config.yaml"), "foo", OK);
            verifyNoMoreInteractions(changeListener, eventListener);
            reset(changeListener, eventListener);

            var config2 = ArgumentCaptor.forClass(TestConfig.class);
            doThrow(new IllegalArgumentException("foo"))
                    .when(changeListener)
                    .onConfigChange(eq(ConfigChangeType.DELETED), any(TestConfig.class));

            try (var update = cm.update()) {
                update.delete("config.yaml");
            }

            verify(changeListener, timeout(250))
                    .onConfigChange(eq(ConfigChangeType.DELETED), config2.capture());
            assertThat(config2.getValue(), is(config.getValue()));
            verify(eventListener).onConfigFileRead(
                    FileEvent.DELETED, tmp.resolve("config.yaml"), OK);
            verify(eventListener, timeout(250)).onConfigFileUpdate(
                    ConfigChangeType.DELETED, tmp.resolve("config.yaml"), "foo", ERROR);
            verifyNoMoreInteractions(changeListener, eventListener);
        }
    }

    @Test
    public void testUpdateException_OnCreated() throws IOException, ConfigException {
        var cm = new TestConfigMap(tmp);
        try (var update = cm.update()) {
            update.writeContent(
                    "config.yaml",
                    "name: \"foo\"\n" +
                    "value: \"bar\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        try (var watcher = new ConfigWatcher<>(
                tmp,
                new YamlConfigReader<>(TestConfig.class, y -> {}),
                cfg -> cfg.name)) {
            watcher.addEventListener(eventListener);
            watcher.addChangeListener(changeListener);
            watcher.start();

            var config = ArgumentCaptor.forClass(TestConfig.class);

            verify(changeListener, timeout(250)).onConfigChange(eq(ConfigChangeType.CREATED), config.capture());
            assertThat(config.getValue().name, is("foo"));
            assertThat(config.getValue().value, is("bar"));
            verify(eventListener).onConfigFileRead(FileEvent.CREATED, tmp.resolve("config.yaml"), OK);
            verify(eventListener).onConfigFileUpdate(ConfigChangeType.CREATED, tmp.resolve("config.yaml"), "foo", OK);
            verifyNoMoreInteractions(changeListener, eventListener);
            reset(changeListener, eventListener);

            var config2 = ArgumentCaptor.forClass(TestConfig.class);
            doThrow(new IllegalArgumentException("foo"))
                    .when(changeListener)
                    .onConfigChange(eq(ConfigChangeType.CREATED), any(TestConfig.class));

            try (var update = cm.update()) {
                update.writeContent(
                        "other.yaml",
                        "name: \"fizz\"\n" +
                        "value: \"buzz\"\n");
            }

            // may happen more than once because of the exception.
            verify(changeListener, timeout(250).atLeastOnce())
                    .onConfigChange(eq(ConfigChangeType.CREATED), config2.capture());
            assertThat(config2.getValue().name, is("fizz"));
            assertThat(config2.getValue().value, is("buzz"));
            verify(eventListener, atLeastOnce()).onConfigFileRead(
                    any(), eq(tmp.resolve("config.yaml")), eq(OK));
            verify(eventListener, atLeastOnce()).onConfigFileRead(
                    FileEvent.CREATED, tmp.resolve("other.yaml"), OK);
            verify(eventListener, atLeastOnce()).onConfigFileUpdate(
                    ConfigChangeType.CREATED, tmp.resolve("other.yaml"), "fizz", ERROR);
            verifyNoMoreInteractions(changeListener, eventListener);
        }
    }
}
