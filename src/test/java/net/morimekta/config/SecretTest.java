package net.morimekta.config;

import net.morimekta.collect.util.Binary;
import net.morimekta.testing.file.TestConfigMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;

import java.io.IOException;
import java.nio.file.Path;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.writeString;
import static net.morimekta.collect.util.Binary.encodeFromString;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class SecretTest {
    @TempDir
    public Path config;
    @TempDir
    public Path secretsDir;

    @Test
    public void testSecretLoadedFromManager() throws IOException, InterruptedException, ConfigException {
        var secrets = new TestConfigMap(secretsDir);
        try (var update = secrets.update()) {
            writeString(
                    update.path("TEST"),
                    "PASSWORD");
            writeString(
                    update.path("OTHER"),
                    "p4ssw0rd");
        }
        writeString(
                config.resolve("test-config.yml"),
                "secretsManager: \"" + secretsDir.toString() + "\"\n" +
                "secret: \"${TEST}\"\n" +
                "nullValue: null\n" +
                "fromEnv: \"${PATH}\"\n" +
                "unknown: \"OTHER\"\n" +
                "secretEnv: \"${PATH}\"\n" +
                "secretValue: \"${OTHER}\"\n" +
                "nullSecret: null\n" +
                "rawSecret: \"FOO\"");

        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class)) {
            loader.load(config.resolve("test-config.yml"));
            var loaded = loader.get();
            assertThat(loaded, is(notNullValue()));
            assertThat(loaded.secretsManager, is(notNullValue()));
            assertThat(loaded.secretsManager.getSecretsPath(), is(secretsDir));
            assertThat(loaded.secret.getName(), is("TEST"));
            assertThat(loaded.secret.getAsString(), is("PASSWORD"));

            assertThat(loaded.secretsManager.get("TEST").getName(), is("TEST"));
            assertThat(loaded.secretsManager.getAsString("TEST"), is("PASSWORD"));
            assertThat(loaded.secretsManager.getAsBytes("TEST"), is(new byte[]{80, 65, 83, 83, 87, 79, 82, 68}));
            assertThat(loaded.secretsManager.getAsBinary("TEST"), is(Binary.fromHexString("50415353574f5244")));

            assertThat(loaded.nullValue, is(nullValue()));
            assertThat(loaded.fromEnv, is(notNullValue()));
            assertThat(loaded.fromEnv.getName(), containsString("${PATH}"));
            assertThat(loaded.fromEnv.getAsString(), containsString("/usr/bin"));
            assertThat(loaded.unknown, is(notNullValue()));
            assertThat(loaded.unknown.getName(), is("OTHER"));
            assertThat(loaded.unknown.getAsString(), is("p4ssw0rd"));
            assertThat(loaded.secret.toString(), is("Secret{'TEST'}"));
            assertThat(loaded.secret.hashCode(), is(not(loaded.fromEnv.hashCode())));
            assertThat(loaded.secret, is(not(loaded.fromEnv)));
            assertThat(loaded.secret, is(loaded.secret));
            assertThat(loaded.secret, is(new Secret("TEST", false, Binary.fromHexString("50415353574f5244"))));
            assertThat(loaded.secret, is(notNullValue()));
            assertThat(loaded.secret, is(not("TEST")));
            assertThat(loaded.fromEnv.toString(), is("Secret{'${PATH}'}"));

            assertThat(loaded.secretValue, is("p4ssw0rd"));
            assertThat(loaded.secretEnv, containsString("/usr/bin"));
            assertThat(loaded.rawSecret, is("FOO"));
            assertThat(loaded.nullSecret, is(nullValue()));

            try (var update = secrets.update()) {
                writeString(
                        update.path("TEST"),
                        "PASSWORD-2");
            }

            Thread.sleep(100);

            assertThat(loaded.secret.getAsString(), is("PASSWORD-2"));
            assertThat(loaded.secretsManager.getAsString("TEST"), is("PASSWORD-2"));
        }
    }

    public static Stream<Arguments> dataSecretLoad_Fails() {
        return Stream.of(
                arguments("secret: \"TEST\"\n",
                          "No secrets manager to load secret: TEST\n" +
                          " at [Source: (BufferedReader); line: 1, column: 9]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"),
                arguments("secretsManager: \"SECRETS_DIR\"\n" +
                          "secret: \"TEST\"\n",
                          "Unknown secret TEST\n" +
                          " at [Source: (BufferedReader); line: 2, column: 9]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"),
                arguments("secretsManager: \"SECRETS_DIR\"\n" +
                          "secret: \"${TEST}\"\n",
                          "Unknown secret ${TEST}\n" +
                          " at [Source: (BufferedReader); line: 2, column: 9]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"),
                arguments("secretsManager: \"SECRETS_DIR\"\n" +
                          "secret: \"DUST\"\n",
                          "Unknown secret DUST\n" +
                          " at [Source: (BufferedReader); line: 2, column: 9]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"),
                arguments("secretsManager: \"SECRETS_DIR\"\n" +
                          "secret: \"${DUST}\"\n",
                          "Unknown secret ${DUST}\n" +
                          " at [Source: (BufferedReader); line: 2, column: 9]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"),
                arguments("secretsManager: \"SECRETS_DIR\"\n" +
                          "secret: \"\"\n",
                          "Empty secret name.\n" +
                          " at [Source: (BufferedReader); line: 2, column: 9]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"),
                arguments("secretsManager: \"SECRETS_DIR\"\n" +
                          "secret: 42\n",
                          "Invalid Secret token 42, expected string\n" +
                          " at [Source: (BufferedReader); line: 2, column: 9]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"));
    }

    @ParameterizedTest
    @MethodSource("dataSecretLoad_Fails")
    public void testSecretLoad_Fails(String configContent, String expectedMessage) throws IOException {
        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class)) {
            writeString(
                    config.resolve("test-config.yml"),
                    configContent.replaceAll("SECRETS_DIR", secretsDir.toString()));
            try {
                loader.load(config.resolve("test-config.yml"));
                fail("No exception: " + expectedMessage);
            } catch (ConfigException e) {
                assertThat(e.getMessage(), is(expectedMessage));
            }
            writeString(
                    config.resolve("test-config.yml"),
                    "secretsManager: \"" + secretsDir.toString() + "\"\n" +
                    "secret: \"DUST\"\n");
            try {
                loader.load(config.resolve("test-config.yml"));
                fail("No exception");
            } catch (ConfigException e) {
                assertThat(e.getMessage(), is(
                        "Unknown secret DUST\n" +
                        " at [Source: (BufferedReader); line: 2, column: 9]" +
                        " (through reference chain: net.morimekta.config.TestConfig[\"secret\"])"));
            }
        }
    }

    @Test
    public void testConfigNoSecretsManager() throws IOException {
        writeString(
                config.resolve("test-config.yml"),
                "fromEnv: \"${PATH}\"\n");

        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class)) {
            loader.loadUnchecked(config.resolve("test-config.yml"));
            var loaded = loader.get();
            assertThat(loaded, is(notNullValue()));

            assertThat(loaded.fromEnv, is(notNullValue()));
            assertThat(loaded.fromEnv.getName(), containsString("${PATH}"));
            assertThat(loaded.fromEnv.getAsString(), containsString("/usr/bin"));
        }
    }

    @Test
    public void testListener() {
        SecretListener a = Mockito.mock(SecretListener.class);
        SecretListener b = Mockito.mock(SecretListener.class);

        Secret secret = new Secret("FOO", false, encodeFromString("BAR", UTF_8));
        secret.addListener(a);
        secret.addListener(b);

        doThrow(new IllegalArgumentException("foo")).when(a).onSecretChange(any());

        secret.setSecret(encodeFromString("BAZ", UTF_8));

        verify(a).onSecretChange(secret);
        verify(b).onSecretChange(secret);
        verifyNoMoreInteractions(a, b);
        reset(a, b);

        secret.removeListener(b);

        secret.setSecret(encodeFromString("YYZ", UTF_8));

        verify(a).onSecretChange(secret);
        verifyNoMoreInteractions(a, b);
    }
}
