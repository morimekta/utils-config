package net.morimekta.config.readers;

import net.morimekta.config.ConfigException;
import net.morimekta.config.TestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class YamlConfigReaderTest {
    @TempDir
    public Path tmp;

    @Test
    public void testReadConfigFail() throws IOException, ConfigException {
        var reader = new YamlConfigReader<>(
                TestConfig.class, mapper -> {});
        try {
            fail("No exception: " + reader.readConfig(null));
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("file == null"));
        }
        var file = tmp.resolve("file.yaml");
        try {
            fail("No exception: " + reader.readConfig(file));
        } catch (FileNotFoundException e) {
            assertThat(e.getMessage(), is("No such config file " + file));
        }
        var dir = tmp.resolve("dir");
        Files.createDirectory(dir);
        try {
            fail("No exception: " + reader.readConfig(dir));
        } catch (IOException e) {
            assertThat(e.getMessage(), is("Config path " + dir + " is not a regular file."));
        }
        Files.writeString(file, "not yaml at all...", CREATE_NEW);
        try {
            fail("No exception: " + reader.readConfig(file));
        } catch (ConfigException e) {
            assertThat(
                    e.getMessage(),
                    is("Cannot construct instance of `net.morimekta.config.TestConfig`" +
                       " (although at least one Creator exists): no String-argument constructor/factory method to" +
                       " deserialize from String value ('not yaml at all...')\n" +
                       " at [Source: (BufferedReader); line: 1, column: 1]"));
        }
    }
}
