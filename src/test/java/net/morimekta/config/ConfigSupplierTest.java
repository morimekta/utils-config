package net.morimekta.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import net.morimekta.config.ConfigEventListener.Status;
import net.morimekta.config.readers.FixedConfigReaderSupplier;
import net.morimekta.config.readers.YamlConfigReader;
import net.morimekta.file.FileEvent;
import net.morimekta.file.FileEventListener;
import net.morimekta.file.FileWatcher;
import net.morimekta.testing.concurrent.FakeClock;
import net.morimekta.testing.file.TestConfigMap;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.mockito.ArgumentCaptor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;

import static java.nio.file.Files.writeString;
import static net.morimekta.config.ConfigChangeType.CREATED;
import static net.morimekta.config.ConfigChangeType.DELETED;
import static net.morimekta.config.ConfigChangeType.MODIFIED;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class ConfigSupplierTest {
    @TempDir
    public Path config;

    @Test
    public void testConfigSupplier() {
        var loader = ConfigSupplier.yamlConfig(TestConfig.class);

        try {
            fail("No exception: " + loader.get());
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("Config not loaded."));
        }

        assertThat(loader.toString(), is("ConfigSupplier{config=null}"));
    }

    @Test
    public void testConfigSupplier_Unchecked() throws IOException {
        writeString(
                config.resolve("test-config.yml"),
                "fromEnv: \"${VARIABLE_DOES_NOT_EXIST}\"\n");
        Files.createDirectory(config.resolve("dir"));

        var loader = ConfigSupplier.config(TestConfig.class);

        try {
            fail("No exception: " + loader.get());
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("Config not loaded."));
        }

        try {
            loader.loadUnchecked(config.resolve("test-config.yml"));
            fail("No exception");
        } catch (UncheckedConfigException e) {
            assertThat(e.getMessage(),
                       is("Unknown secret ${VARIABLE_DOES_NOT_EXIST}\n" +
                          " at [Source: (BufferedReader); line: 1, column: 10]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"fromEnv\"])"));
        }

        try {
            loader.loadAndMonitorUnchecked(config.resolve("test-config.yml"));
            fail("No exception");
        } catch (UncheckedConfigException e) {
            assertThat(e.getMessage(),
                       is("Unknown secret ${VARIABLE_DOES_NOT_EXIST}\n" +
                          " at [Source: (BufferedReader); line: 1, column: 10]" +
                          " (through reference chain: net.morimekta.config.TestConfig[\"fromEnv\"])"));
        }

        try {
            loader.loadUnchecked(config.resolve("missing.yml"));
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("No such config file " + config + "/missing.yml"));
            assertThat(e.getCause(), is(instanceOf(FileNotFoundException.class)));
        }

        try {
            loader.loadAndMonitorUnchecked(config.resolve("missing.yml"));
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("No such config file " + config + "/missing.yml"));
            assertThat(e.getCause(), is(instanceOf(FileNotFoundException.class)));
        }

        try {
            loader.loadUnchecked(config.resolve("dir"));
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("Config path " + config + "/dir is not a regular file."));
            assertThat(e.getCause(), is(instanceOf(IOException.class)));
        }

        try {
            loader.loadAndMonitorUnchecked(config.resolve("dir"));
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("Config path " + config + "/dir is not a regular file."));
            assertThat(e.getCause(), is(instanceOf(IOException.class)));
        }
    }

    @Test
    public void testConfigSupplier_PreLoaded() throws IOException, ConfigException {
        writeString(
                config.resolve("test-config.yml"),
                "name: \"NAME\"\n");
        try (var loader = ConfigSupplier.config(TestConfig.class, config.resolve("test-config.yml"))) {
            assertThat(loader.get(), is(new TestConfig("NAME", null)));
        }
    }

    @Test
    public void testConfigSupplier_PreLoadedFails() throws IOException, ConfigException {
        var file = config.resolve("test-config.yml");
        try (var loader = ConfigSupplier.config(TestConfig.class, file)) {
            fail("no exception: " + loader.get());
        } catch (FileNotFoundException e) {
            assertThat(e.getMessage(), is("No such config file " + file));
        }
    }

    @Test
    public void testConfigSupplier_PreLoadedYaml() throws IOException, ConfigException {
        writeString(
                config.resolve("test-config.yml"),
                "name: \"NAME\"\n");
        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class,
                                                    config.resolve("test-config.yml"),
                                                    ObjectMapper::findAndRegisterModules)) {
            assertThat(loader.get(), is(new TestConfig("NAME", null)));
        }
    }

    @Test
    public void testConfigSupplier_PreLoadedYamlFails() throws IOException, ConfigException {
        var file = config.resolve("test-config.yml");
        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class,
                                                    file,
                                                    ObjectMapper::findAndRegisterModules)) {
            fail("no exception: " + loader.get());
        } catch (FileNotFoundException e) {
            assertThat(e.getMessage(), is("No such config file " + file));
        }
    }

    @Test
    public void testConfigSupplier_NoReaderProvided() throws IOException {
        var file = config.resolve("test-config.prototext");
        writeString(
                file,
                "name: \"NAME\"\n");
        try (var loader = ConfigSupplier.config(TestConfig.class, file)) {
            fail("no exception: " + loader.get());
        } catch (ConfigException e) {
            assertThat(e.getMessage(),
                       is("No config reader available for test-config.prototext of type net.morimekta.config.TestConfig"));
        }
    }

    @Test
    public void testConfigNoSecretsManager() throws IOException {
        writeString(
                config.resolve("test-config.yml"),
                "fromEnv: \"${PATH}\"\n");

        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class, ObjectMapper::findAndRegisterModules)) {
            loader.loadUnchecked(config.resolve("test-config.yml"));
            var loaded = loader.get();
            assertThat(loaded, is(notNullValue()));

            assertThat(loaded.fromEnv, is(notNullValue()));
            assertThat(loaded.fromEnv.getName(), containsString("${PATH}"));
            assertThat(loaded.fromEnv.getAsString(), containsString("/usr/bin"));
        }
    }

    @Test
    public void testConfigMonitoring() throws IOException, ConfigException {
        var clock = new FakeClock();
        var cm = new TestConfigMap(config);
        try (var update = cm.update()) {
            writeString(
                    update.path("test-config.yml"),
                    "value: \"first\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        var file = config.resolve("test-config.yml");
        try (var supplier = new ConfigSupplier<>(new FixedConfigReaderSupplier<>(new YamlConfigReader<>(TestConfig.class,
                                                                                                        i -> {})),
                                                 ConfigWatcher.FILE_WATCHER,
                                                 clock)
                .addEventListener(eventListener)
                .addChangeListener(changeListener)) {
            supplier.loadAndMonitorUnchecked(config.resolve("test-config.yml"));
            try {
                supplier.loadUnchecked(config.resolve("test-config.yml"));
                fail("No exception");
            } catch (IllegalStateException e) {
                assertThat(e.getMessage(), is("Config already loaded."));
            }

            assertThat(supplier.getFile(), is(file));
            assertThat(supplier.getLastUpdatedAt(), is(clock.instant()));
            clock.tick(3, TimeUnit.MINUTES);

            var asCreated = supplier.get();
            assertThat(asCreated.value, is("first"));

            verify(eventListener).onConfigFileRead(FileEvent.CREATED, file, Status.OK);
            verify(changeListener).onConfigChange(CREATED, asCreated);
            verify(eventListener).onConfigFileUpdate(CREATED, file, "test-config.yml", Status.OK);
            verifyNoMoreInteractions(eventListener, changeListener);
            reset(eventListener, changeListener);

            try (var update = cm.update()) {
                writeString(
                        update.path("test-config.yml"),
                        "value: \"second\"\n");
            }

            var updateCaptor = ArgumentCaptor.forClass(TestConfig.class);
            verify(eventListener, timeout(250)).onConfigFileRead(FileEvent.MODIFIED, file, Status.OK);
            verify(changeListener, timeout(250)).onConfigChange(eq(MODIFIED), updateCaptor.capture());
            verify(eventListener, timeout(250)).onConfigFileUpdate(MODIFIED, file, "test-config.yml", Status.OK);
            verifyNoMoreInteractions(eventListener, changeListener);
            reset(eventListener, changeListener);

            assertThat(updateCaptor.getValue(), is(notNullValue()));
            assertThat(updateCaptor.getValue().value, is("second"));

            assertThat(supplier.getFile(), is(file));
            assertThat(supplier.getLastUpdatedAt(), is(clock.instant()));

            var asUpdated = supplier.get();
            assertThat(asUpdated, is(updateCaptor.getValue()));

            try (var update = cm.update()) {
                writeString(
                        update.path("test-config.yml"),
                        "value: - \"invalid\"?\n");
            }

            verify(eventListener, timeout(250)).onConfigFileRead(FileEvent.MODIFIED, file, Status.PARSE_FAILED);
            verifyNoMoreInteractions(eventListener, changeListener);
            reset(eventListener, changeListener);

            doThrow(new IllegalArgumentException("foo"))
                    .when(changeListener).onConfigChange(any(), any());
            doThrow(new IllegalArgumentException("foo"))
                    .when(eventListener).onConfigFileRead(any(), any(), any());
            doThrow(new IllegalArgumentException("foo"))
                    .when(eventListener).onConfigFileUpdate(any(), any(), anyString(), any());

            try (var update = cm.update()) {
                update.delete("test-config.yml");
            }

            var deleteCaptor = ArgumentCaptor.forClass(TestConfig.class);
            verify(eventListener, timeout(250)).onConfigFileRead(FileEvent.DELETED, file, Status.OK);
            verify(changeListener, timeout(250)).onConfigChange(eq(DELETED), deleteCaptor.capture());
            verify(eventListener, timeout(250)).onConfigFileUpdate(DELETED, file, "test-config.yml", Status.ERROR);
            verifyNoMoreInteractions(eventListener, changeListener);

            assertThat(deleteCaptor.getValue(), is(asUpdated));
            assertThat(supplier.get(), is(asUpdated));
        }
    }

    @Test
    public void testOnFileEvent() throws IOException, ConfigException {
        var cm = new TestConfigMap(config);
        try (var update = cm.update()) {
            writeString(
                    update.path("test-config.yml"),
                    "value: \"first\"\n");
        }

        @SuppressWarnings("unchecked")
        var changeListener = (ConfigChangeListener<TestConfig>) mock(ConfigChangeListener.class);
        var eventListener = mock(ConfigEventListener.class);
        var fileWatcher = mock(FileWatcher.class);
        var file = config.resolve("test-config.yml");
        try (var supplier = new ConfigSupplier<>(new YamlConfigReader<>(TestConfig.class, i -> {}),
                                                 () -> fileWatcher)
                .addEventListener(eventListener)
                .addChangeListener(changeListener)) {
            var fileEventCapture = ArgumentCaptor.forClass(FileEventListener.class);

            supplier.loadAndMonitorUnchecked(config.resolve("test-config.yml"));

            verify(fileWatcher).weakAddWatcher(eq(file), fileEventCapture.capture());
            verify(eventListener).onConfigFileRead(FileEvent.CREATED, file, Status.OK);
            verify(eventListener).onConfigFileUpdate(CREATED, file, "test-config.yml", Status.OK);
            verify(changeListener).onConfigChange(eq(CREATED), any(TestConfig.class));
            verifyNoMoreInteractions(fileWatcher, eventListener, changeListener);
            reset(fileWatcher, eventListener, changeListener);

            var fileEventListener = fileEventCapture.getValue();

            // not a file.
            fileEventListener.onFileEvent(config.resolve("nope"), FileEvent.CREATED);

            verifyNoMoreInteractions(fileWatcher, eventListener, changeListener);
        }
    }

    @Test
    public void testConfigNoConfigFile() {
        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class)) {
            loader.loadUnchecked(config.resolve("test-config.yml"));
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("No such config file " + config.toString() + "/test-config.yml"));
        }
    }

    @Test
    public void testConfigNotAFile() {
        try (var loader = ConfigSupplier.yamlConfig(TestConfig.class)) {
            loader.loadUnchecked(config);
            fail("No exception");
        } catch (UncheckedIOException e) {
            assertThat(e.getMessage(), is("Config path " + config + " is not a regular file."));
        }
    }
}
