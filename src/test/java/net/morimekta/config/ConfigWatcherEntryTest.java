package net.morimekta.config;

import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.time.Instant;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ConfigWatcherEntryTest {


    @Test
    public void testConfigEntry() {
        var path = Path.of("/tmp");
        var time = Instant.ofEpochMilli(1234567890123L);

        var config1 = new TestConfig();
        var config2 = new TestConfig("foo", "bar");
        var config3 = new TestConfig("fizz", "buzz");
        var a = new ConfigWatcher.ConfigEntry<>(path.resolve("foo"), "foo", time, config1);
        var a2 = new ConfigWatcher.ConfigEntry<>(path.resolve("foo"), "foo", time, config1);
        var b = new ConfigWatcher.ConfigEntry<>(path.resolve("bar"), "bar", time, config2);
        var c = new ConfigWatcher.ConfigEntry<>(path.resolve("foo"), "foo", time, config3);

        assertThat(a.hashCode(), is(not(b.hashCode())));
        assertThat(a.hashCode(), is(a2.hashCode()));

        assertThat(a, is(a));
        assertThat(a, is(a2));
        assertThat(a, is(not("foo")));
        assertThat(a, is(not(nullValue())));
        assertThat(a, is(not(b)));
        assertThat(a, is(not(c)));

        assertThat(a.toString(),
                   is("ConfigEntry{file=/tmp/foo, identifier='foo', updatedAt=2009-02-13T23:31:30.123Z, config=TestConfig{}}"));
    }
}
